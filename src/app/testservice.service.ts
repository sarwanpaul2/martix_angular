import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TestserviceService {

  constructor(private http:HttpClient) { }
  private _registerUrl = "https://rwwyisdsvwpwbuo.matrixm.io/api/register";
  private _imageUrl = "https://rwwyisdsvwpwbuo.matrixm.io/api/imageupload";

  addRecord(data):Observable<any>{
    return this.http.post(this._registerUrl, data);
  }
  
  uploadImage(data):Observable<any>{
    console.log(data);
    return this.http.post(this._imageUrl, data);
  }
}
