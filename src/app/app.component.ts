import { Component, OnInit } from '@angular/core';
import { TestserviceService } from './testservice.service'; 
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'larvlanglr';
  selectedFile:any;
  imageUrl:any;
  submitFormData:any;
  customData={};
  arrayData = [];

  fieldsName=[];
  getFieldName=[];

  jsonData=[{"status":"success","layout":[{"id":1,"system_resource_id":"1","system_class_id":"0","system_class_name":"","sections":[{"id":1,"layout_id":1,"section_name":"General","section_sort_order":2,"section_fields":"18,20,21,10,1","show_section_title":"no","section_fields_details":[{"id":18,"name":"beds","display_name":"Beds"},{"id":20,"name":"address","display_name":"Address"},{"id":21,"name":"baths","display_name":"Baths"},{"id":10,"name":"list_price","display_name":"List Price"},{"id":1,"name":"mls_num","display_name":"MLS Number"}]},{"id":3,"layout_id":1,"section_name":"Location","section_sort_order":3,"section_fields":"44,16,30","show_section_title":"no","section_fields_details":[{"id":44,"name":"city","display_name":"City"},{"id":16,"name":"elementary_school","display_name":"Elementary School"},{"id":30,"name":"high_school","display_name":"High School"}]},{"id":4,"layout_id":1,"section_name":"Description","section_sort_order":4,"section_fields":"28","show_section_title":"no","section_fields_details":[{"id":28,"name":"description","display_name":"Description"}]}]}]}];

  sectionData:any[]=[];

  datas0=[];datas1=[];datas2=[];

  constructor(private addUserService: TestserviceService, private toaster:ToastrService, private spinner: NgxSpinnerService) {
  
  }
ngOnInit(){

  /** Start Task 7 */
  var objectData={
    "Commercial":[
      {
        "Business": "Business"
      }, {
        "Business Only": "Business"
      }, {
        "Commercial Condo": "Condo"
      }, {
        "Commercial Retail": "Condo"
      }
    ],
    "Multi-Unit": [
      {
        "5-8 Units": "Units"
      }, 
      {
        "9-12 Units": "Units"
      }, 
      {
        "Over 12 Units": "Units"
      }
    ],
    "Residential": [
      {
        "Co-Op": "Co-Op"
      }, 
      {
        "Condominium": "Condo"
      }, 
      {
        "Cottage / Rec. Properties": "Condo"
      }
    ]
  }
  
  var last = Object.keys(objectData).pop();
  this.customData[last] = objectData[last];

  var keys = Object.keys(objectData);
  var values = Object.values(objectData);

  for(var i = 0; i<keys.length; i++){
    if(i != 2){
      this.customData[keys[i]] = values[i];
    }   
  }
  
  /** End Task 7 */

  /** Start Task 8 */
  
  if(this.jsonData[0] && this.jsonData[0].layout[0] && this.jsonData[0].layout[0].sections){
    this.sectionData = this.jsonData[0].layout[0].sections;

    this.sectionData.forEach((elementValue, elementKey) => {
      console.log(elementKey);
      console.log(elementValue.section_fields_details);
      elementValue.section_fields_details.forEach((value,key) =>{
        //console.log(value.name);
        if(elementKey == 0){
          this.datas0.push(value.display_name);
        }
        if(elementKey == 1){
          this.datas1.push(value.display_name);
        }
        if(elementKey == 2){
          this.datas2.push(value.display_name);
        }
      });
    });
    
  }
  
  /** End Task */


  
}

uploadImage(event){
  this.spinner.show();
  const fd = new FormData();
  this.selectedFile = <File>event.target.files[0],
  fd.append('profile_picture', this.selectedFile, this.selectedFile.name);
  
  this.addUserService.uploadImage(fd).subscribe(
    (response) => {
      if(response.data.status == 200){
        this.spinner.hide();
        this.imageUrl = response.data.image;
        this.toaster.success(response.data.message);
      }
    },
    (error) => {
      this.spinner.hide();
      console.log(error);
    }
  );
}

taskSubmit(data){
    this.spinner.show();
    this.submitFormData = data;
    this.submitFormData['profile_picture'] = this.imageUrl;
  
    this.addUserService.addRecord(this.submitFormData).subscribe(
      (response) => {
        this.spinner.hide();
        this.toaster.success(response.success.message);
      },
      (error) => {
        this.spinner.hide();
        console.log(error);
      }
    )
}





}
